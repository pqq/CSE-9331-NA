import java.io.*;
import java.net.*;
import java.util.*;



public class Http {

	boolean debug				= false;		// set this to true to print out debug information
	boolean overwriteFiles		= true;		// overwrite already downloaded files with same name
	boolean displayAnchors		= true;		// print anchors to screen
	boolean downloadSrcObjects	= true;		// download the src objects in a page
	boolean downloadJpgLinks	= false;	// if true, all '.jpg' anchors are downloaded (f.ex all big pictures referenced to by a index page)
	
	String _url;							// the URL we try to download
	String _port;							// the portnumber of the webserver
	String _username;						// username/login-name
	String _password;						// password
	String _server;							// servername we download URL from (extracted from URL)

	String _proxyServer;					// proxy settings...
	String _proxyPort;
	String _proxyUsername;
	String _proxyPassword;

	String _object;							// objectname we try to download (extracted from URL)
	DataOutputStream outToServer;			// the bytes we are going to send to the server
	DataInputStream inFromServer;			// the bytes we are going to read from the server
	String _htmlObject="";					// The HTML text we download is stored in this object.
	String _stat="";						// Used for storing statistic of a download

	String _downloadDirectory = "./";		// download path of all downloaded objects

	boolean _secure;						// secure download?
	boolean _proxy;							// proxy download?

	// constructor - download
	public Http(String url){
		_url 	= url;
		_port 	= "80";
		_server = getServernameFromUrl(url);
		_object = getObjectnameFromUrl(url);

		_secure = false;
		_proxy = false;

		downloadUrl();
	}

	// constructor - secure_download
	public Http(String url, String username, String password){
		_url 		= url;
		_port 		= "80";
		_server 	= getServernameFromUrl(url);
		_object 	= getObjectnameFromUrl(url);
		_username 	= username;
		_password 	= password;

		_secure 	= true;
		_proxy 		= false;

		downloadUrl();
	}


	// constructor - proxy_download
	public Http(String url, String proxyServer, String proxyPort, String proxyUsername, String proxyPassword){
		_url = url;
		_port 			= "80";
		_server 		= getServernameFromUrl(url);
		_object 		= getObjectnameFromUrl(url);
		_proxyServer	= proxyServer;
		_proxyPort		= proxyPort;
		_proxyUsername	= proxyUsername;
		_proxyPassword	= proxyPassword;

		_secure 		= false;
		_proxy 			= true;

		proxyDownloadUrl();
	}


	// constructor - secure_proxy_download
	public Http(String url, String proxyServer, String proxyPort, String proxyUsername, String proxyPassword, String username, String password){
		_url = url;
		_server = getServernameFromUrl(url);
		_object = getObjectnameFromUrl(url);
		_proxyServer	= proxyServer;
		_proxyPort		= proxyPort;
		_proxyUsername	= proxyUsername;
		_proxyPassword	= proxyPassword;
		_username 		= username;
		_password		= password;

		_secure 		= true;
		_proxy 			= true;

		proxyDownloadUrl();
	}




	// open connection and download the url
	void downloadUrl(){
		try{

			String authString = "";

			if (_secure)
				authString = Base64.encode(_username+":"+_password);			

			debug("Downloading URL '"+_url+"':");
			debug("server: "+_server);
			debug("object: "+_object);

			Socket socket = new Socket(_server, Integer.parseInt(_port));

			outToServer = new DataOutputStream(socket.getOutputStream());
			inFromServer = new DataInputStream(socket.getInputStream());

			send("GET "+_object+" HTTP/1.1");
			send("Connection: Close");
			if (_secure)
				send("Authorization: Basic "+authString);
			send("Host: "+_server);
			send("");

			receiveMultiple();

			socket.close();

		} catch (UnknownHostException e){
			print("Unkown host: "+_server);
		} catch (BindException e){
			print("Socket binding problem");
		} catch (Exception e) {		
			e.printStackTrace();
		}
	}






	// open connection and download the url through a proxy server
	void proxyDownloadUrl(){
		try{

			String authString = "";
			String proxyAuthString = "";
			
			proxyAuthString = Base64.encode(_proxyUsername+":"+_proxyPassword);
			if (_secure)
				authString = Base64.encode(_username+":"+_password);			

			debug("Downloading URL '"+_url+"':");
			debug("server: "+_server);
			debug("object: "+_object);
			debug("through proxy: "+_proxyServer+":"+_proxyPort);

			Socket socket = new Socket(_proxyServer, Integer.parseInt(_proxyPort));

			outToServer = new DataOutputStream(socket.getOutputStream());
			inFromServer = new DataInputStream(socket.getInputStream());

			send("GET http://"+_server+_object+" HTTP/1.1");
			send("Proxy-Connection: Close");
			send("Proxy-Authorization: Basic "+proxyAuthString);
			if (_secure)
				send("Authorization: Basic "+authString);
			send("Host: "+_server);
			send("");

			receiveMultiple();

			socket.close();

		} catch (UnknownHostException e){
			print("Unkown proxy server: "+_proxyServer);
		} catch (BindException e){
			print("Socket binding problem");
		} catch (Exception e) {		
			e.printStackTrace();
		}
	}








	// send a command to the server
    void send (String cmdline) {
		try {
			debug(">>" + cmdline);
			outToServer.writeBytes(cmdline + "\r\n");
		} catch (Exception e) {
			e.printStackTrace();	
		}
    }




	// receive multiple lines
    void receiveMultiple() {
		try {
			String line 			= "";			// one line from the server
			String statusCode		= "";			// status-code
			String contentType		= "";			// content-type

			int lineNo 				= 0;			// line number

			byte b					= 0;			// byte read from server

			int newlinebytesize 	= 0;			// how many bytes that makes a newline
		    int totalbytesize		= 0;			// total size ob object;
		    int chunkbytesize		= 0;			// size of the object
			long totalchunksize		= 0;			// the datasize of a chunk

		    Date timer;								// used to receive a time
		    long starttime			= 0;			// starttime
		    long endtime			= 0;			// endtime
		    long downloadtime		= 0;			// donwnloadtime (endtime-starttime)

		    boolean finished 		= false;		// if true, we are finished receiving data from server
		    boolean ctypeFound		= false;		// if true, content-type is specified in header
		    boolean body			= false;		// true when we have found the body of the data received
			boolean chunkedTransfer = false;		// is true when we will look for chunk-datasize instead of processing a normal line
			boolean chunkSizeFound	= false;		// true when we have found the chunksize
			boolean countLine		= true;			// a boolean wich becomes false if we want count the line size
			boolean newLineFound	= false;		// true when we find newline


			/* --------------------------------------------------
			 * Example of Chunked-Encoding (Chunked Transfer)
			 * --------------------------------------------------
			 * [HEADER]
			 * chunk-size (in HEX)		<-- stored in "totalchunksize", chunkSizeFound = true
			 * [CHUNK-OF-BODY]			<-- the size of this part is stored in "chunkbytesize"
			 * chunk-size
			 * [CHUNK-OF-BODY]
			 * chunk-size == 0			<-- always ends with chunksize equal to zero
			 * EOF
			 * --------------------------------------------------
			 */

			// stat: store url
			if (_url.startsWith("http://"))
				saveStat(_url);
			else 
				saveStat("http://"+_url);


			timer = new Date();
			starttime = timer.getTime();			// get timestamp for start


		    while (!finished) {
				countLine = true;					// reset to true every round...
		    	lineNo++;							// linecounter...

				// -------------------------------------------------------------------
				// read one line from server, and store it in "line" variable
				// -------------------------------------------------------------------
				line = "";							// reset line
				newlinebytesize = 0;				// reset newlinebytesize
				while (!newLineFound){
					try{
						b = inFromServer.readByte();
						if (b==10){					// new line
							newLineFound=true;
							if (body)
								newlinebytesize++;
						} else if (b==13){			// carriage return
							if (body)
								newlinebytesize++;
						} else {
							line = line+Character.toString((char)b);
						}
					} catch (EOFException e) {
						newLineFound = true;
						if (line.length()==0){
							finished = true;
							lineNo--;				// decreas linecounter, sinze this was an empty line
						}
					}
				}
				newLineFound =false;				// reset newlinefound


				debug(line);

				// ---------------------------------------------------------
				// go through "line", first look for relevant header info
				// ---------------------------------------------------------
				if (lineNo==1){
					statusCode = parse(line," ")[1];
					if (!statusCode.startsWith("2")){
						print("Download error: "+line);
						return;
					}
				}	
				
				if (!finished && line.startsWith("Content-Type: ") && !body){				// saves the content-type if we find this in the header
					contentType = parse(line," ")[1];
					contentType = contentType.replaceAll(";","");
					saveStat(contentType);													// stat: save contentType
					ctypeFound = true;														// update boolean
				}

				if (!finished && line.equals("Transfer-Encoding: chunked") && !body){		// we might have a chuncked transfer
					chunkedTransfer = true;
				}

				// -------------------------------------
				// look for start of body
				// -------------------------------------
				// If chunked-transfer, then body starts after we have found first chunk
				if (chunkedTransfer && !body && !chunkSizeFound && !finished && line.length()==0){
					body = true;
					countLine = false;
				} else if (!chunkedTransfer && !body && !finished && line.length()==0){
					body = true;
					countLine = false;
				}

				// -----------------------------------------
				// Chunked Transfer: find size of chunk
				// -----------------------------------------
				if (chunkedTransfer && body && !chunkSizeFound && !finished && line.length()!=0 && !line.startsWith(" ")){
					countLine = false;									// this is a chunk-line, so don't count with this in bytesize
					chunkSizeFound = true;								// we have found the size of this chunk
					chunkbytesize=0;									// resets everytime we find a new chunk of data
					totalchunksize = Hex2Dec.convert(line.trim());		// convert hex to decimal
					totalbytesize += totalchunksize;					// store total-size as a sum of all chunk-sizes
					if (totalchunksize==0)								// if chunksize is equal to zero, we are at the end
						finished=true;
				}


				// ------------------------------------
				// Count number of bytes received/parsed
				// ------------------------------------
				if (body && !finished){
					// chunkedTransfer - count if we have found a "chunkSize" (and if countLine)
					if (chunkedTransfer && countLine && chunkSizeFound){
						chunkbytesize += line.length() + newlinebytesize;
					}
					// normal transfer/encoding - count every byte in the body (if countLine)
					else if (!chunkedTransfer && countLine){
						totalbytesize += line.length() + newlinebytesize;
					}					
				}
				
				// if it's a chunkedTransfer and we have found all bytes in this part, then we have to look for a new chunksize
				if (chunkedTransfer && chunkbytesize>=totalchunksize){
					chunkSizeFound = false;
				}



				// ---------------------------------------------
				// store the body into an HTML object
				// ----------------------------------------------
				if (line!=null && body){
					// put the entire HTML code in one string without linebreak.
					_htmlObject = _htmlObject.concat(line+" ");
				}				
			} // while (!finished)

			timer = new Date();
			endtime = timer.getTime();					// get timestamp for end

			if (!ctypeFound)							// store content-type=N/A (Not Available) if we didn't receive this
				saveStat("N/A");
				
			saveStat(Integer.toString(totalbytesize));	// stat: filesize
			
			downloadtime = endtime - starttime;
			saveStat(Long.toString(downloadtime)+"\n");	// stat: downloadtime
			
			_htmlObject = removeComments(_htmlObject);	// remove all comments from HTML code
			if (displayAnchors)
				printAnchors(_htmlObject);				// display anchors (a hrefs)
			if (downloadSrcObjects)
				getSrcObjects(_htmlObject);				// download src-objects

		} catch (Exception e) {
			e.printStackTrace();	
		}
    }



	String getServernameFromUrl(String url){
		int index;
	
		url = url.replaceAll("http://","");

		index = url.indexOf('/');
		if (index > -1)
			url = url.substring(0,index);
		
		return url;
	}



	String getObjectnameFromUrl(String url){
		int index;
		
		url = url.replaceAll("http://","");

		index = url.indexOf("/");
		if (index > -1){
			url = url.substring(index);
		} else {
			url = "/";
		}
		return url;
	}




    String[] parse(String str, String delim){
		String[] tokens = null;
		int i=0;
				
	    StringTokenizer st = new StringTokenizer(str,delim);
		tokens = new String[100];

	    while (st.hasMoreTokens()) {
			tokens[i] = new String(st.nextToken());
			i++;
	    }
		return tokens;
    }




	String removeComments(String line){
		int index=0, index2=0, index_=0;
		boolean finished = false;
		String commentfreeString = "";

		while(!finished){
			index = line.substring(index_).toLowerCase().indexOf("<!--");
			if ( index>-1 ){
				index = index+index_;
				index2 = line.substring(index).toLowerCase().indexOf("-->");
				if (index2>-1){
					index2 = index2 + index;
					//System.out.println("comment in substr from "+index+" to "+index2+"");
					//print(line.substring(index,index2+3));
					commentfreeString = commentfreeString.concat(line.substring(index_,index));
					index_ = index2+3; // saves to check if there are more comments after this index point
				} else
					finished = true;
			} else
				finished = true;
		} // while (!finished)
		
		
		// add the part after last comment
		if (index_!=0){
			commentfreeString = commentfreeString.concat( line.substring(index_,line.length()) );
		} else {
			commentfreeString = line;
		}
		return commentfreeString;
	}





	void printAnchors(String line){
		int index=0, index2=0, index_=0;
		String anchor;
		boolean finished = false;
		while(!finished){
			index = line.substring(index_).toLowerCase().indexOf("<a ");
			if ( index>-1 ){
				index = index+index_;
				index2 = line.substring(index).toLowerCase().indexOf("href");
				if (index2>-1){
					index2 = index2 + index;
					index = line.substring(index2).indexOf("=");
					if (index>-1){
						index = index + index2;
						index2 = line.substring(index).indexOf("\"");
						if (index2>-1){
							index2 = index2 + index;
							index = line.substring(index2+1).indexOf("\"");
							if (index>-1){
								index = index + index2+1;
								//System.out.println("href in substr from "+index2+" to "+index+"");
								index_ = index; // saves to check if there are more hrefs after this index point
								anchor = line.substring(index2+1,index);
								// replace double/multiple space with single space in anchors
								while (anchor.indexOf("  ")>-1)
									anchor = anchor.replaceAll("  "," ");
								print(anchor);
								if (downloadJpgLinks && 
									(
										anchor.endsWith(".jpg") ||
										anchor.endsWith(".jpeg")
									)
									){
									downloadObject(anchor);
								}
							} else
								finished = true;
						} else
							finished = true;
					} else
						finished = true;
				} else
					finished = true;
			} else
				finished = true;
		} // while (!finished)		
	}






	void getSrcObjects(String line){
		int index=0, index2=0, index_=0;
		boolean finished = false;

		while(!finished){
			index2 = line.substring(index_).toLowerCase().indexOf("<");
			if (index2>-1){
				index2 = index2 + index;
				index = line.substring(index_).toLowerCase().indexOf("img");
				if ( index>-1 ){
					index = index+index_;
					index2 = line.substring(index).toLowerCase().indexOf("src");
					if (index2>-1){
						index2 = index2 + index;
						index = line.substring(index2).indexOf("=");
						if (index>-1){
							index = index + index2;
							index2 = line.substring(index).indexOf("\"");
							if (index2>-1){
								index2 = index2 + index;
								index = line.substring(index2+1).indexOf("\"");
								if (index>-1){
									index = index + index2+1;
									//System.out.println("href in substr from "+index2+" to "+index+"");
									debug(line.substring(index2+1,index));
									index_ = index; // saves to check if there are more srcs after this index point
									downloadObject(line.substring(index2+1,index));
								} else
									finished = true;
							} else
								finished = true;
						}
					} else
						finished = true;
				} else
					finished = true;
			}  else
				finished = true;
		} // while (!finished)
	}



	void downloadObject(String url){
		try{
			String authString = "";
			String proxyAuthString = "";
			Socket socket;
			String filename = url;
			String orgUrl = _url;
			int index;
			int index2;
			String server;

			// it might be a complete url, then we don't change url, only server in case the objects is on another one...
			if (url.startsWith("http://")){
				server = getServernameFromUrl(url);
				url = url.replaceAll("http://","");
			} else {

				// the original url might start with 'http://' - then we have to remove that part
				orgUrl = orgUrl.replaceAll("http://","");
	
				// if url starts with '/' the object is on the server root, else we
				// create url to object by concatinating everything before the last '/' from _url (original url)
				// with relative url to object			
				if (url.startsWith("/")){
					url = _server + url;
				} else {
					index = orgUrl.lastIndexOf("/");
					if (index>-1)
						url = orgUrl.substring(0,index+1)+""+url;
					else
						url = _server+"/"+url;
				}
				server = _server;
			}
						
			// saves everything after the last '/' as filename
			index = filename.lastIndexOf("/");
			if (index>-1)
				filename = filename.substring(index+1,filename.length());
			
			if (_proxy){
				proxyAuthString = Base64.encode(_proxyUsername+":"+_proxyPassword);
			}
			
			if (_secure) {
				authString = Base64.encode(_username+":"+_password);
			}


			// fixes '/..' in url
			index = url.indexOf("/..");
			while (index>-1){
				index2 = url.substring(0,index).lastIndexOf("/");
				url = url.substring(0,index2).concat( url.substring(index+3,url.length()) );
				index = url.indexOf("/..");
			}

			// removes double '//' - might occure sometimes...
			url = url.replaceAll("//","/");


			debug("Downloading Object '"+url+"' from '"+server+"'");
			
			if (_proxy) {
				debug("through proxy: "+_proxyServer+":"+_proxyPort);
			}

			if (_proxy){
				socket = new Socket(_proxyServer, Integer.parseInt(_proxyPort));
			} else {
				socket = new Socket(_server, Integer.parseInt(_port));
			}

			outToServer = new DataOutputStream(socket.getOutputStream());
			//inFromServer = new BufferedReader( new InputStreamReader(socket.getInputStream()) );

			send("GET http://"+url+" HTTP/1.1");
			if (_proxy){
				send("Proxy-Connection: Close");
				send("Proxy-Authorization: Basic "+proxyAuthString);
			} else {
				send("Connection: Close");			
			}
			
			if (_secure) {
				send("Authorization: Basic "+authString);
			}

			send("Host: "+server);
			send("");


			DataInputStream bytesFromServer = new DataInputStream(socket.getInputStream());
			saveStat("http://"+url);							// stat: store url
			receiveObject(bytesFromServer,filename);
			
			socket.close();

		} catch (UnknownHostException e){
			print("Unkown proxy server: "+_proxyServer);
		} catch (BindException e){
			print("Socket binding problem");
		} catch (Exception e) {		
			e.printStackTrace();
		}
	}







    void receiveObject(DataInputStream bytesFromServer, String filename) {
		try {
			byte b								= 0;
		    boolean finished 					= false;
		    FileOutputStream fileOutputStream	= null;
		    int byteCounter 					= 0;
		    boolean fosOpen 					= false;
			boolean firstLine					= true;
			boolean firstSpaceFound				= false;
			boolean statuscodeFound				= false;
			int body							= 0;
		    long starttime						= 0;
		    long endtime						= 0;
		    long downloadtime					= 0;
		    Date timer;


			// set appropriate content-type of object
			if (filename.toLowerCase().endsWith(".jpg") || filename.toLowerCase().endsWith(".jpeg"))
				saveStat("image/jpeg");
			else if (filename.toLowerCase().endsWith(".gif"))
				saveStat("image/gif");
			else if (filename.toLowerCase().endsWith(".png"))
				saveStat("image/png");
			else
				saveStat("N/A");

			timer = new Date();
			starttime = timer.getTime();

			File destFile;
			destFile = new File(_downloadDirectory+filename);
			if ( destFile.exists() && !overwriteFiles){
				debug("Object already downloaded: "+filename);
				finished=true;
			} else {
				debug("write data to file: '"+filename+"'");
				try {
					destFile.createNewFile();
				} catch (IOException e) {
					debug("Not a valid filename: '"+filename+"', object not downloaded");
					finished = true;
				}
				if (!finished){
					fileOutputStream = new FileOutputStream(destFile);
					fosOpen = true;
				}
			}

			if (debug && !finished)
				System.out.print("DEBUG: <<Downloading: ");


		    while (!finished) {
				try {
					b = bytesFromServer.readByte();
				} catch (EOFException e) {
					finished = true;
				}
				
				// Header is always on this form:
				// HTTP/1.1 200 OK
				
		    	if (b==13) // 13 = ASCII value for 'carriage return'
		    		firstLine = false;

		    	if (firstLine && firstSpaceFound && !statuscodeFound){
		    		statuscodeFound = true;
		    		if (b!=50){ // '2'
		    			debug("Unable to download object: "+filename);
		    			saveStat("\n");
		    			return;
		    		}
		    	}
		    	
		    	if (firstLine){
		    		if (b==32) // 'space'
		    			firstSpaceFound = true;
		    	}
		    	
				if (!finished && body==2){
					if (debug)
           				System.out.print("*");
       				byteCounter++;
					fileOutputStream.write(b);
				}

		    	// heading always ends with two carriage returns
		    	// a new line is represented by first '13' (carriage ret.) then '10' (new line)
		    	if (body != 2){
			    	if (b==10){
			    		body++;
			    	} else if (b!=13){
			    		body=0;
			    	}
			    }

			} // while (!finished)

			timer = new Date();
			endtime = timer.getTime();

			if (fosOpen){
				fileOutputStream.flush();
				fileOutputStream.close();
			}
			
			saveStat(Integer.toString(byteCounter));
			debug(byteCounter+" bytes was written to file");

			downloadtime = endtime - starttime;
			saveStat(Long.toString(downloadtime)+"\n");

		} catch (Exception e) {
			e.printStackTrace();	
		}
    }






	void debug (String line){
		if (debug)
			System.out.println("DEBUG: "+line);
	}


	void print (String line){
			System.out.println(line);
	}

	void saveStat(String line){
		if (_stat.equals("") || _stat.endsWith("\n"))
			_stat = _stat+line;
		else
			_stat = _stat+" "+line;
	}


	String getStat(){
		return _stat;
	}

}

